﻿
using UnityEngine;
using System.Collections;

public class _playerMovement : MonoBehaviour 
{

	public float speed = 10;
	int jumpHeight = 1000;
	int jumpSpeed = 20;

	//	private float targetSpeed;
	private float currentSpeed; //keeps track of player's speed

    float horizontal;
	float vertical;

	private Vector2 movement;
	private Vector2 jumpMovement;

//	private AnimationState idle;
//	private AnimationState run;
//	private AnimationState jump;

	bool jumping = false;

	void Start () 
	{
		if (!GetComponent<NetworkView>().isMine)
		{
			Component.Destroy(this);
		}
		//		animation.wrapMode = WrapMode.Loop;
		//		animation["jump"].wrapMode = WrapMode.Once;
		//		animation ["jump"].layer = 1;
		//
		//		idle = animation["140_07"];
		//		run = animation["02_03"];
		//		jump = animation["02_04"];
	}

	void Update()
	{
		GetComponent<Animation>().CrossFade ("140_07");
	}
	

	void FixedUpdate () 
	{
		horizontal = Input.GetAxis ("Horizontal") * speed;
//		animation.Play ("02_03", PlayMode.StopAll);
		movement = new Vector2 (horizontal, GetComponent<Rigidbody>().velocity.y);
		GetComponent<Rigidbody>().velocity = movement;

		if(Input.GetKeyDown(KeyCode.A))
		{
//			animation.Play("02_03", PlayMode.StopAll);
			transform.Rotate(0,180,0);
		}
		else if(Input.GetKeyUp (KeyCode.A))
		{
			transform.Rotate (0,-180,0);
		}

		if (Input.GetKey (KeyCode.Space) && jumping == false) 
		{
//			animation.Play ("02_04");
			GetComponent<Rigidbody>().AddForce (Vector3.up * jumpHeight * jumpSpeed);
			Physics.gravity = new Vector3 (0, -25.0f, 0);
			jumping = true;
		}
	}

	void OnCollisionEnter(Collision c)
	{
		if (c.gameObject.tag == "ground")
		{
			jumping = false;
		}

		if (c.gameObject.tag == "firecollider")
		{
			Debug.Log ("You Have Died");
			Application.LoadLevel ("gameOverScreen");
		}
	}
}
