﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour
{
	//PICKUPS
	public List<GameObject> bombPickUp = new List<GameObject> ();
	public GameObject bombPrefab;
	GameObject[] bombSpawn;
	public bool[] bombSpawnsTaken;

	public float bombSpawnAreaGive = 1f;

	//ENEMIES
	public List<GameObject> enemies = new List<GameObject>();
    public GameObject enemyPrefab;
    GameObject[] enemySpawns;
    public bool[] enemySpawnsTaken;

    public float spawnAreaGive = 1f;

    //counter for how many enemies have been created
    int enemyCounter = 0;
	int bombCounter = 0;

    // Use this for initialization
    void Start()
    {
		//BOMB/PICKUPS*************************************************************
		//find any object with tag "bombPickUp"
		bombSpawn = GameObject.FindGameObjectsWithTag ("bombPickUp");

		//check if anything has been taken is its spawn
		bombSpawnsTaken = new bool[bombSpawn.Length];

		//Instantiates new bomb every 'x' amount of seconds
		InvokeRepeating ("CreateBombAgain", 20, 20);



		//ENEMIES******************************************************************
		//Array to store each GameObject with the "enemySpawnPoint" attached to it tag
        enemySpawns = GameObject.FindGameObjectsWithTag("enemySpawnPoint");

		//to check if any spawn points have been taken
        enemySpawnsTaken = new bool[enemySpawns.Length];

		//Creates a new enemy every 'x' amount of seconds
        InvokeRepeating("CreateRandomEnemy", 8, 8);
    }

    // Update is called once per frame
    void Update()
    {

    }

	//ENEMIES
    public void RecreateEnemies(int amount)
    {
        for (int i = 0; i < amount; i++)
        {
            CreateRandomEnemy();
        }
    }

	//BOMB
	public void RecreateBomb(int amount)
	{
		for (int i = 0; i < amount; i++)
		{
			CreateBombAgain();
		}
	}

	//ENEMIES
	public void CreateRandomEnemy()
    {
        bool slotCheck = true;

        for (int i = 0; i < enemySpawnsTaken.Length; i++)
        {
            slotCheck = enemySpawnsTaken[i] && slotCheck;
        }

        if (slotCheck)
        {
            return;
        }

        int slot = Random.Range(0, enemySpawnsTaken.Length - 1);

        while (enemySpawnsTaken[slot])
        {
            slot = (slot + 1) % enemySpawnsTaken.Length;
        }

        //create enemies and give name
        GameObject enemy;
        enemy = Instantiate(enemyPrefab, new Vector3(enemySpawns[slot].transform.position.x + Random.Range(-spawnAreaGive, spawnAreaGive),
                                     				 enemySpawns[slot].transform.position.y + Random.Range(-spawnAreaGive, spawnAreaGive)),
                             		 				 enemySpawns[slot].transform.rotation) as GameObject;

        enemySpawnsTaken[slot] = true;
        enemy.GetComponent<Enemy>().spawnIndex = slot;

        enemyCounter++;
        enemy.name = "Enemy" + enemyCounter;
    }

	public void CreateBombAgain ()
	{
		bool spawnSpotCheck = true;

		for (int i = 0; i < bombSpawnsTaken.Length; i++)
		{
			spawnSpotCheck = bombSpawnsTaken[i] && spawnSpotCheck;
		}

		if (spawnSpotCheck)
		{
			return;
		}

		int spawnSpot = Random.Range (0, bombSpawnsTaken.Length - 1);

		while (bombSpawnsTaken[spawnSpot])
		{
			spawnSpot = (spawnSpot + 1) % bombSpawnsTaken.Length;
		}

		//where we instantiate object
		GameObject bomb;
		bomb = Instantiate(enemyPrefab, new Vector3(bombSpawn[spawnSpot].transform.position.x + Random.Range(-bombSpawnAreaGive, bombSpawnAreaGive),
		                                            bombSpawn[spawnSpot].transform.position.y + Random.Range(-bombSpawnAreaGive, bombSpawnAreaGive)),
		                   							bombSpawn[spawnSpot].transform.rotation) as GameObject;
		
		bombSpawnsTaken[spawnSpot] = true;
		bomb.GetComponent<bombItem>().spawnIndex = spawnSpot;
		
		bombCounter++;
		bomb.name = "bomb" + bombCounter;
	}
}
