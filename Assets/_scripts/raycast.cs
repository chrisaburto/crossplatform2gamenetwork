﻿using UnityEngine;
using System.Collections;

public class raycast : MonoBehaviour
{

	public GameObject Character;
	private RaycastHit Hit;

	void Start ()
	{
	
	}
	

	void Update ()
	{
		Vector2 myTransform = Character.transform.forward;

		if(Input.GetKey(KeyCode.Mouse0))
		{
			if(Physics.Raycast(transform.position, myTransform, out Hit, 10))
			{
				if(Hit.collider.gameObject.CompareTag("enemy"))
				{
					Debug.DrawLine(transform.position, Character.transform.forward, Color.white);
					Debug.Log("ray detected an object");

					Destroy(Hit.collider.gameObject);
					Debug.Log ("You've Destroyed one of the targets");
				}
			}
		}
	}
}
