﻿/* Sorry for the late submisson, I've been very sick since friday
 * being infront of the screen caused severe headaches. Anyway, What I 
 * have completed is the basic movements (run, walk, jump)
 * and also raycast shooting that uses the left mouse button and only
 * works when the player is facing forward so you cannot shoot backwards.
 * I also added spawning enemies that can be destroyed when you get near them
 * and shoot.
 * 
 * 
 * 
 * things I had a little trouble with are:
 * 
 * randomly spawning only 1-2 enemies on any of the 5 spawn points I have
 * layed out. Instead i've been only able to spawn 5 at once (one at each
 * spawn point).
 * 
 * 
 * respawning them after they/or one of them are dead, I implemented
 * a respawn function found in "_enemySpawn" where there is a boolean 
 * that tells you whether the enemy is destroyed or not. So when I made an
 * if statement saying that if the boolean == true, then respawn them again,
 * and then set the boolean back to false. I can't seem to figure out why
 * it hasnt been working properly.
 * 
 * rotating the players position when A or D is pressed, it only partially
 * works
 * 
 * and animation, under _playerMovement I added animation.Play() and then
 * the name of the animation and it doesnt seem to be working. The player
 * has animator and animation components with a controller.
 * */