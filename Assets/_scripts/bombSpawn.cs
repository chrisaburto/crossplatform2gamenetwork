﻿using UnityEngine;
using System.Collections;

public class bombSpawn : MonoBehaviour
{
	public GameObject Character;
	private RaycastHit Hit;
	public int bombSpawnAmount = 3;

	GameManager gm;

	bool destroyed = false;

	void Start ()
	{
		gm = GameObject.Find("GameManager").GetComponent<GameManager>();
	}

	void OnTriggerEnter(Collider col)
    {
        Debug.Log(col.gameObject.name);

		if (col.gameObject.name.StartsWith("Player") && (Network.isClient || Network.isServer))
		{
			print("TRIGGER");
			GetComponent<NetworkView>().RPC("Jump", RPCMode.AllBuffered, 600f);
		}
		if (col.gameObject.name == "DefaultAvatar")
        {
            gm.RecreateBomb(bombSpawnAmount);
            //outputs to let me know that the player has entered the trigger and that it is working correctly
            //it then destroys the trigger incase the player walks into it again
            Debug.Log("Player triggered the enemySpawnTrigger!");
            Destroy(gameObject);
        }
    }


	void Update ()
	{
		Vector2 myTransform = Character.transform.forward;
		
		if (Input.GetKey(KeyCode.Mouse0))
		{
			if (Physics.Raycast(transform.position, myTransform, out Hit, 10))
			{
				if (Hit.collider.gameObject.CompareTag("bombPickUp"))
				{
					Debug.DrawLine(transform.position, Character.transform.forward, Color.white);
					Debug.Log("ray detected an object");
					
					Destroy(Hit.collider.gameObject);
					Debug.Log("You've Destroyed one of the targets");
					
					destroyed = true;
				}
			}
		}
	}
}