﻿using UnityEngine;
using System.Collections;

public class _enemySpawn : MonoBehaviour
{

    public GameObject Character;
    private RaycastHit Hit;
    public int SpawnAmount = 5;

    GameManager gm;

    //if enemy is destroyed
    bool destroyed = false;


    void Start()
    {
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
    }


    //All player triggers
    void OnTriggerEnter(Collider col)
    {
        Debug.Log(col.gameObject.name);
        if (col.gameObject.name == "DefaultAvatar")
        {
            gm.RecreateEnemies(SpawnAmount);
            //outputs to let me know that the player has entered the trigger and that it is working correctly
            //it then destroys the trigger incase the player walks into it again
            Debug.Log("Player triggered the enemySpawnTrigger!");
            Destroy(gameObject);
        }
    }

    //Raycast script, draws a line and destroys anything with tags "enemy"
    void Update()
    {
        Vector2 myTransform = Character.transform.forward;

        if (Input.GetKey(KeyCode.Mouse0))
        {
            if (Physics.Raycast(transform.position, myTransform, out Hit, 10))
            {
                if (Hit.collider.gameObject.CompareTag("enemy"))
                {
                    Debug.DrawLine(transform.position, Character.transform.forward, Color.white);
                    Debug.Log("ray detected an object");

                    Destroy(Hit.collider.gameObject);
                    Debug.Log("You've Destroyed one of the targets");

                    destroyed = true;
                }
            }
        }
    }
}

//Transform enemy;
//GameObject enemySpawnPoint = GameObject.Find("spawnPoint1");
//enemy = Instantiate(Enemy, enemySpawnPoint.transform.position, enemySpawnPoint.transform.rotation) as Transform;