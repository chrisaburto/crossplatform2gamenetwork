﻿using UnityEngine;
using System.Collections;

public class bombItem : MonoBehaviour
{
	GameManager gm;

	[HideInInspector]
	public int spawnIndex = -1;
	
	void Start()
	{
		gm = GameObject.Find ("GameManager").GetComponent<GameManager> ();
		gm.bombPickUp.Add (gameObject);
	}
	
	void Update ()
	{
	
	}

	void OnDestroy()
	{
		gm.bombPickUp.Remove(gameObject);
		gm.bombSpawnsTaken[spawnIndex] = false;
	}
}