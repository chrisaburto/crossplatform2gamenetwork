﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour 
{
	GameManager gm;

    [HideInInspector]
    public int spawnIndex = -1;

	void Start()
    {
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
        gm.enemies.Add(gameObject);
    }
	
	void Update () 
	{
	
	}
	
	void OnDestroy()
    {
        gm.enemies.Remove(gameObject);
        gm.enemySpawnsTaken[spawnIndex] = false;
    }
}