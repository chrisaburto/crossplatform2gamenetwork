I had a few problems with the bomb pick up and spawning the player with networking implemented:

- I created a bomb pick up that should be respawning between 3 different spawn points, using the same system that was created for the enemies, no errors but bomb does not spawn

- and some problems spawning the character when clicking start server, and sometimes it would say registration successful, other times it would say it failed.